import assertk.all
import assertk.assertThat
import assertk.assertions.containsExactly
import assertk.assertions.isEqualTo
import assertk.assertions.prop
import org.approvaltests.Approvals
import org.example.catcoder.bowling.Bowling
import org.example.catcoder.bowling.Input
import org.junit.jupiter.api.Test

class BowlingTest {
    private fun repl(input: String): String {
        return Bowling.repl(input)
    }

    @Test
    fun `parse input`() {
        assertThat(Bowling.read("3:1,4,6,4,7,0")).all {
            prop(Input::rounds).isEqualTo(3)
            prop(Input::throws).containsExactly(1, 4, 6, 4, 7, 0)
        }
    }

    @Test
    fun `verify some approvals`() {
        val input =
            listOf(
                "1:0,0",
                "1:1,0",
                "1:0,1",
                "2:0,0,0,0",
                "2:1,0,0,0",
                "2:0,1,0,0",
                "2:0,0,1,0",
                "2:0,0,0,1",
                "1:1,4",
                "2:6,4,0,0",
                "2:6,4,1,0",
                "2:6,4,0,1",
                "3:1,4,6,4,7,0",
                "3:0,0,9,1,0,0",
                "10:1,4,4,5,6,4,5,5,10,0,1,7,3,6,4,10,2,8,6",
            )

        Approvals.verifyAll("", input.map { "$it => ${repl(it)}" })
    }

    @Test
    fun `level 1`() {
        val input =
            listOf(
                "3:1,2,6,4,5,2",
                "2:1,2,6,4,5",
                "1:2,8,5",
                "3:0,0,9,1,0,0",
            )

        Approvals.verifyAll("", input.map { "$it => ${repl(it)}" })
    }

    @Test
    fun `level 2`() {
        val input =
            listOf(
                "4:1,5,5,5,4,6,8,1",
                "3:1,5,5,5,4,6,8",
            )

        Approvals.verifyAll("", input.map { "$it => ${repl(it)}" })
    }

    @Test
    fun `level 3`() {
        val input =
            listOf(
                "3:3,4,10,1,2",
                "2:3,4,10,1,2",
            )

        Approvals.verifyAll("", input.map { "$it => ${repl(it)}" })
    }

    @Test
    fun `level 4`() {
        val input =
            listOf(
                "4:1,5,10,10,1,7",
                "3:1,5,10,10,1,7",
            )

        Approvals.verifyAll("", input.map { "$it => ${repl(it)}" })
    }

    @Test
    fun `level 5`() {
        val input =
            listOf(
                "4:2,7,10,4,6,4,5",
                "4:2,7,4,6,10,4,5",
                "3:2,7,4,6,10,4,5",
            )

        Approvals.verifyAll("", input.map { "$it => ${repl(it)}" })
    }

    @Test
    fun `level 6`() {
        val input =
            listOf(
                "10:1,4,4,5,6,4,5,5,10,0,1,7,3,6,4,10,2,8,6",
                "10:0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0",
                "10:10,10,10,10,10,10,10,10,10,10,10,10",
                "10:7,2,1,9,6,4,5,5,10,3,7,7,3,6,4,10,2,8,6",
            )

        Approvals.verifyAll("", input.map { "$it => ${repl(it)}" })
    }
}
