package org.example.catcoder.bowling

import assertk.all
import assertk.assertThat
import assertk.assertions.*
import io.kotest.core.spec.style.FunSpec
import io.kotest.datatest.withData
import io.kotest.property.Arb
import io.kotest.property.arbitrary.arbitrary
import io.kotest.property.arbitrary.list
import io.kotest.property.arbitrary.nonNegativeInt
import io.kotest.property.checkAll

data class Input(val rounds: Int, val throws: List<Int>)

data class Result(val scores: List<Int>)

const val ALL_PINS = 10

object Bowling {
    fun read(input: String): Input {
        val (first, second) = input.split(":")
        return Input(first.toInt(), second.split(",").map(String::toInt))
    }

    fun eval(input: Input): Result {
        val scores = mutableListOf<Int>()
        var score = 0
        val pinsThrown = input.throws.listIterator()
        for (i in 1..input.rounds) {
            val first = pinsThrown.next()
            val second = pinsThrown.next(0)
            val third = pinsThrown.peek(0)
            score += first + second
            if (first + second >= ALL_PINS) {
                score += third
            }
            if (first == ALL_PINS) {
                pinsThrown.previous()
            }
            scores.add(score)
        }
        return Result(scores)
    }

    fun ListIterator<Int>.next(default: Int): Int {
        return when {
            hasNext() -> next()
            else -> default
        }
    }

    fun ListIterator<Int>.peek(default: Int): Int {
        return when {
            hasNext() -> next().also { previous() }
            else -> default
        }
    }

    fun print(result: Result): String {
        return result.scores.joinToString(",")
    }

    fun repl(input: String): String {
        return input.let(this::read).let(this::eval).let(this::print)
    }
}

class BowlingFunSpecTest : FunSpec({
    test("parse input") {
        assertThat(Bowling.read("3:1,4,6,4,7,0")).all {
            prop(Input::rounds).isEqualTo(3)
            prop(Input::throws).containsExactly(1, 4, 6, 4, 7, 0)
        }
    }

    context("known examples") {
        withData(
            "3:1,4,6,4,7,0" to "5,22,29",
            "10:10,10,10,10,10,10,10,10,10,10,10,10" to "30,60,90,120,150,180,210,240,270,300",
        ) { (input, expected) ->
            assertThat(Bowling.repl(input)).isEqualTo(expected)
        }
    }

    test("should not contain spaces") {
        checkAll(Arb.rounds()) { input ->
            assertThat(Bowling.repl(bowlingInput(input))).doesNotContain(" ")
        }
    }

    test("should only contain positive integers") {
        checkAll(Arb.rounds()) { input ->
            val scores = Bowling.repl(bowlingInput(input)).split(",").map(String::toInt)
            assertThat(scores).each {
                it.isGreaterThanOrEqualTo(0)
            }
        }
    }

    test("should contain the same amount of scores as there are rounds") {
        checkAll(Arb.rounds()) { input ->
            val scores = Bowling.repl(bowlingInput(input)).split(",").map(String::toInt)
            assertThat(scores).hasSize(input.size)
        }
    }

    test("should only increase") {
        checkAll(Arb.rounds()) { input ->
            val scores = Bowling.repl(bowlingInput(input)).split(",").map(String::toInt)
            assertThat(scores).isEqualTo(scores.sorted())
        }
    }

    test("should not exceed 300") {
        checkAll(Arb.rounds()) { input ->
            val scores = Bowling.repl(bowlingInput(input)).split(",").map(String::toInt)
            assertThat(scores.last()).isLessThanOrEqualTo(300)
        }
    }

    test("should be at least the amount of pins") {
        checkAll(Arb.rounds()) { input ->
            val scores = Bowling.repl(bowlingInput(input)).split(",").map(String::toInt)
            assertThat(scores.last()).isGreaterThanOrEqualTo(input.sumOf { it.first })
        }
    }

    test("should be at least the some of pins in the first round") {
        checkAll(Arb.rounds()) { input ->
            val firstRound = input.first()
            val scores = Bowling.repl(bowlingInput(input)).split(",").map(String::toInt)
            assertThat(scores.first()).isGreaterThanOrEqualTo(firstRound.first + firstRound.second)
        }
    }
})

fun Arb.Companion.round(): Arb<Triple<Int, Int, Int>> =
    arbitrary {
        val first = Arb.nonNegativeInt(ALL_PINS).bind()
        val second = Arb.nonNegativeInt(ALL_PINS - first).bind()
        val last = Arb.nonNegativeInt(ALL_PINS).bind()

        Triple(first, second, last)
    }

fun Arb.Companion.rounds(): Arb<List<Triple<Int, Int, Int>>> = Arb.list(Arb.round(), 1..10)

fun bowlingInput(rounds: List<Triple<Int, Int, Int>>): String {
    val throws = mutableListOf<Int>()
    rounds.forEachIndexed { index, (first, second, third) ->
        throws.add(first)
        if (first != ALL_PINS) {
            throws.add(second)
        }
        if (index == 9 && first + second >= ALL_PINS) {
            throws.add(third)
        }
    }

    return "${rounds.size}:${throws.joinToString(",")}"
}
